# MindWave Classifier API v1

Classifier for my Master Thesis in Computer Science in 2020. :mortar_board: <br />
API predicts ***login procedure familiarity*** based on EEG data collected by <a href="http://developer.neurosky.com">`NeuroSky MindWave BCI device`</a>. <br />
>>>
**General purpose**: Increase security of IT systems, by new-factor authentication with brain waves activity.
>>>

API link: 
<a href ="https://mindwaveclassifierapi.azurewebsites.net/swagger/index.html"> Azure App Service API - Swagger </a>

![](/screens/swagger.png)

## Endpoints:

![](/screens/post.png)

### Request format

```javascript
EEGData{
Delta*	            number($float)
Theta*	            number($float)
Alpha_low*          number($float)
Alpha_high*         number($float)
Beta_low*           number($float)
Beta_high*          number($float)
Gamma_low*          number($float)
Gamma_high*         number($float)
Attention*          number($float)
Meditation*         number($float)
BlinkStrength*	    number($float)
}
```

### Response format

```javascript
EEGPrediction{
Prediction      boolean //Prediction: True if login procedure known by the user, false otherwise
Score           number($float)
Probability     number($float)
}
```

## How it works

API is using <a href="https://dotnet.microsoft.com/apps/machinelearning-ai/ml-dotnet">ML.NET</a> learned data model of more than 2500 records tagged by two flags: **KNOWN** and **UNKNOWN**. <br />
The algorithm that is used to predict results is *LightGbmBinary* with accuracy circa 71,43%

![](/screens/classifier.png)
