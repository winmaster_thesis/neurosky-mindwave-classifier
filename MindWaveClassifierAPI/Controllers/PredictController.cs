﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.ML;
using MindWaveClassifierAPI.DataModels;

namespace MindWaveClassifierAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PredictController : ControllerBase
    {
        private readonly PredictionEnginePool<EEGData, EEGPrediction> _predictionEnginePool;

        public PredictController(PredictionEnginePool<EEGData, EEGPrediction> predictionEnginePool)
        {
            _predictionEnginePool = predictionEnginePool;
        }

        ///  <summary>
        ///  Predicts user's login procedure familiarity based on brain waves data from MindWave.
        ///  </summary>
        /// <param name="input">EEG data from BCI</param>
        /// <returns>EEG prediction data model</returns>
        /// <response code="200">Returns OK status and EEGPrediction data model as a result</response>
        /// <response code="400">If the item is null or model is invalid</response> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<EEGPrediction> Post([FromBody] EEGData input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var prediction = _predictionEnginePool.Predict(modelName: "MLModel", example: input);

            return Ok(prediction);
        }
    }
}