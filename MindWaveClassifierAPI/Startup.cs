﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.ML;
using MindWaveClassifierAPI.DataModels;
using Microsoft.OpenApi.Models;


namespace MindWaveClassifierAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddPredictionEnginePool<EEGData, EEGPrediction>()
                .FromFile(modelName: "MLModel", filePath: "MLModels/FirstMLModel.zip", watchForChanges: true);

            // Register the Swagger generator
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "MindWave Classifier API",
                    Description = "Classifier for my Master Thesis in Computer Science in 2020. <br />API predicts <b>login procedure familiarity </b> based on EEG data collected by NeuroSky MindWave BCI device. <br />General purpose: Increase security of IT systems, by new-factor authentication with brain waves activity.",
                    Contact = new OpenApiContact
                    {
                        Name = "Katarzyna Białas",
                        Url = new Uri("https://gitlab.com/winmaster")
                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                c.IncludeXmlComments("SwaggerAPI.xml");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MindWave Classifier API V1");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseStatusCodePages();
            app.UseStatusCodePages("text/html",
                "<div>\r\n<center>\r\n<div>      \r\n<h1 style=\"color:red; font-size:48px;\">{0}</h1>  \r\n<img src=\"https://i.stack.imgur.com/EzZiD.png\"> \r\n</div>     \r\n<h2><b>Not good...</b></h2> \r\n<h3>Yo know wat you doin?</h3>\r\n<p>This place belongs to:</p>\r\n<a href=\"https://gitlab.com/winmaster\">winmaster</a>\r\n</center>\r\n</div>"); 
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
