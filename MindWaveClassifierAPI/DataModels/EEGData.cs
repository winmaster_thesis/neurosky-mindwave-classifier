﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Microsoft.ML.Data;

namespace MindWaveClassifierAPI.DataModels
{
    [DataContract]
    public class EEGData
    {
        [ColumnName("Flag"), LoadColumn(0)]
        [JsonIgnore]
        public bool Flag { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Delta"), LoadColumn(1)]
        public float Delta { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Theta"), LoadColumn(2)]
        public float Theta { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Alpha_low"), LoadColumn(3)]
        public float Alpha_low { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Alpha_high"), LoadColumn(4)]
        public float Alpha_high { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Beta_low"), LoadColumn(5)]
        public float Beta_low { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Beta_high"), LoadColumn(6)]
        public float Beta_high { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Gamma_low"), LoadColumn(7)]
        public float Gamma_low { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Gamma_high"), LoadColumn(8)]
        public float Gamma_high { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Attention"), LoadColumn(9)]
        public float Attention { get; set; }

        [Required]
        [DataMember]
        [ColumnName("Meditation"), LoadColumn(10)]
        public float Meditation { get; set; }

        [Required]
        [DataMember]
        [ColumnName("BlinkStrength"), LoadColumn(11)]
        public float BlinkStrength { get; set; }
    }
}
