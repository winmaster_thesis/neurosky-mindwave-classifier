﻿using System.ComponentModel;
using System.Runtime.Serialization;
using Microsoft.ML.Data;

namespace MindWaveClassifierAPI.DataModels
{
    [DataContract]
    public class EEGPrediction
    {
        /// <summary>
        /// Prediction: True if login procedure known by the user, false otherwise
        /// </summary>
        [ColumnName("PredictedLabel")]
        [DataMember]
        [Description("True - login procedure known by the user, false otherwise")]
        public bool Prediction { get; set; }

        [DataMember]
        public float Score { get; set; }

        [DataMember]
        public float Probability { get; set; }
    }
}
